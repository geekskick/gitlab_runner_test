#!/bin/bash -ex

docker run \
    --rm \
    -it \
    --mount type=bind,source=$(pwd),target=/repo \
    gittools/gitversion:5.6.6 /repo "${@}"
