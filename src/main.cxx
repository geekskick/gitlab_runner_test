#include <iostream>
#include <string_view>
#include <boost/config.hpp>

namespace{
struct CompilerDetection{
  std::string_view platform{BOOST_PLATFORM};
  std::string_view compiler{BOOST_COMPILER};
  std::string_view date{__DATE__};
};


std::ostream& operator<<(std::ostream& os, const CompilerDetection& cd){
  os << "[" << cd.compiler << "/" << cd.platform << "] " << cd.date;
  return os;
}
}

int main(){
    std::cout << "Hello world, from " << CompilerDetection{} << "\n";
}
